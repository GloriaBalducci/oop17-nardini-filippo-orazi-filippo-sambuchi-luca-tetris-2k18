package main.java.model;

/**
 * Enum that represents the possible shape of a common tetromino.
 */
public enum Shape {
	I,
	L,
	J,
	O,
	S,
	Z,
	T;
}