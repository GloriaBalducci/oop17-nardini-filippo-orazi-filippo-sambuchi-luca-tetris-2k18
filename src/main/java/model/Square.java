package main.java.model;

/**
 * Square is the class representing a filled cell in the game matrix.
 */
public class Square {
	
	private Point2D coords;
	private final SquareColor color;
	
	/**
	 * Class constructor.
	 * 
	 * @param x	x value of the point
	 * @param y y value of the point
	 * @param color	fill color of the square
	 */
	public Square(int x, int y, SquareColor color) {
		this.coords = new Point2D(x, y);
		this.color = color;
	}

	/**
	 * Getter for the coordinates.
	 */
	public Point2D getCoords() {
		return coords;
	}

	/**
	 * Setter for the coordinates.
	 */
	public void setCoords(Point2D coords) {
		this.coords = coords;
	}

	/**
	 * Getter for the color.
	 */
	public SquareColor getColor() {
		return color;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((coords == null) ? 0 : coords.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Square other = (Square) obj;
		if (coords == null) {
			if (other.coords != null) {
				return false;
			}
		} else if (!coords.equals(other.coords)) {
			return false;
		}
		return true;
	}

	public String toString() {
		return this.coords.getX() + "," + this.coords.getY();
	}
}
