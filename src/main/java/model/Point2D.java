package main.java.model;

/**
 * Class representing a 2-dimensional point on a surface.
 */
public class Point2D {
	
	private final int coordinateX;
	private final int coordinateY;
	
	/**
	 * Class constructor.
	 */
	public Point2D(int x, int y) {
		this.coordinateX = x;
		this.coordinateY = y;
	}



	/**
	 * Getter for the x value.
	 */
	public int getX() {
		return coordinateX;
	}

	/**
	 * Getter for the y value.
	 */
	public int getY() {
		return coordinateY;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + coordinateX;
		result = prime * result + coordinateY;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Point2D other = (Point2D) obj;
		if (coordinateX != other.coordinateX) {
			return false;
		}
		if (coordinateY != other.coordinateY) {
			return false;
		}
		return true;
	}
	
}
